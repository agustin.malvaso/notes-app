<?php
return [
    'database' => [
        'host' => 'db',
        'port' => 3306,
        'dbname' => 'notesappdb',
        'charset' => 'utf8mb4'
    ],
];